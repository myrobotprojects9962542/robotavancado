*** Settings ***
Documentation       Exemplos da própria Library: https://github.com/bulkan/robotframework-requests/blob/master/tests/testcase.robot

Library             RequestsLibrary


*** Variables ***
${BASE_URL}     https://petstore.swagger.io


*** Test Cases ***
Exemplo: Pesquisar Pets disponíveis
    Conectar na API da Petstore
    Buscar Pets cujo status é "available"


*** Keywords ***
Conectar na API da Petstore
    Create Session    alias=findPetByStatus    url=${BASE_URL}    disable_warnings=True

Buscar Pets cujo status é "${STATUS}"
    &{PARAMS}    Create Dictionary    status=${STATUS}
    ${RESPONSE}    Get On Session    alias=findPetByStatus    url=/v2/pet/findByStatus    params=${PARAMS}
    Confere sucesso na requisição    ${RESPONSE}

Confere sucesso na requisição
    [Arguments]    ${RESPONSE}
    Should Be True    '${RESPONSE.status_code}'=='200'
    ...    msg=Erro na requisição! Verifique: ${RESPONSE}
