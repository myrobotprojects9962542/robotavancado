*** Settings ***
Library     SeleniumLibrary


*** Variables ***
${DIV_WELCOME}      id=WelcomeContent


*** Keywords ***
#### Conferências
Conferir se os produtos estão sendo exibidos
    Wait Until Element Is Visible    ${DIV_WELCOME}
    Element Should Contain    ${DIV_WELCOME}    Welcome abc!
