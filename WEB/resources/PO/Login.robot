*** Settings ***
Library     SeleniumLibrary
Library     String


*** Variables ***
${HOME_URL}         https://petstore.octoperf.com/actions/Account.action?signonForm=
${IPT_USERNAME}     name=username
${IPT_PASSWORD}     name=password
${BTN_LOGIN}        name=signon
${HOME_TITLE}       JPetStore Demo


*** Keywords ***
#### Ações
Acessar a página home do site
    Go To    ${HOME_URL}
    Wait Until Element Is Visible    ${IPT_USERNAME}    timeout=10
    Title Should Be    ${HOME_TITLE}

Preencher campo username com "${USERNAME}"
    Wait Until Element Is Visible    ${IPT_USERNAME}
    Input Text    ${IPT_USERNAME}    ${USERNAME}

Preencher campo password com "${PASSWORD}"
    Wait Until Element Is Visible    ${IPT_PASSWORD}
    Input Text    ${IPT_PASSWORD}    ${PASSWORD}

Clicar no botão "Login"
    Wait Until Element Is Visible    ${BTN_LOGIN}
    Click Element    ${BTN_LOGIN}
