*** Settings ***
Resource            ./resources/Resource.robot
Resource            ./resources/PO/Login.robot
Resource            ./resources/PO/Home.robot

Test Setup          Abrir navegador
Test Teardown       Fechar navegador


*** Test Cases ***
Caso de Teste com PO 01: Login com credenciais válidas
    Login.Acessar a página home do site
    Login.Preencher campo username com "admin"
    Login.Preencher campo password com "admin"
    Login.Clicar no botão "Login"
    Home.Conferir se os produtos estão sendo exibidos
